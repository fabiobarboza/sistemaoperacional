//
//  FHSOControllerViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/6/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOControllerViewController.h"
#import "FHSOReader.h"
#import "FHSOCode.h"

@interface FHSOControllerViewController ()

@property (nonatomic) FHSOReader* textReader;
@property (weak, nonatomic) IBOutlet UIImageView *tapButton;

@end

@implementation FHSOControllerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)tapEffect
{
    _tapButton.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        _tapButton.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    [UIView animateWithDuration:1.0 animations:^{
        _tapButton.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(tapEffect)
                                   userInfo:nil
                                    repeats:YES];
    //Only for tests
    [self createCode];
}
//Only for tests 
- (void) createCode
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"codeList"] == nil)
    {
        FHSOCode *code = [[FHSOCode alloc] init];
        code.name = @"Sample";
        code.isCurrent = YES;
        NSArray *listinhaDoBruno = [[NSArray alloc] initWithObjects:@"LOAD",@"variable",@"ADD",@"#8",@"SUB",@"#5", @"STORE", @"variable",@"ADD",@"#9", nil];
        code.instructionList = (NSMutableArray*)listinhaDoBruno;
        code.dataList = [[NSMutableArray alloc] initWithObjects:@"variable",@"5", nil];
        code.labelList = [[NSMutableArray alloc] initWithObjects:@"ponto1",@"3", nil];
        [code storeInDefaults];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
