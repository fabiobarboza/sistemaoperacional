//
//  FHSOReader.h
//  sisOp
//
//  Created by Fabio Barboza on 3/6/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHSOReader : NSObject

-(void)readFile;

@end
