//
//  FHSOCode.h
//  sisOp
//
//  Created by Fabio Barboza on 3/14/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHSOCode : NSObject<NSCoding>

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *description;
@property (nonatomic) NSMutableArray *instructionList;
@property (nonatomic) NSMutableArray *dataList;
@property (nonatomic) NSMutableArray *labelList;
@property (nonatomic) NSMutableArray *pagesList;
@property (nonatomic) int pageLength;
@property (nonatomic) int arrivalTime;
@property (nonatomic) int processTime;
@property (nonatomic) int executeTime;
@property (nonatomic) int remainTime; 
@property (nonatomic) int waitingTime;
@property (nonatomic) int lastTime; 
@property (nonatomic) int programCounter;
@property (nonatomic) int acumulator;
@property (nonatomic) int priority;
@property (nonatomic) BOOL isCurrent;
@property (nonatomic) BOOL willExecute; 

+(FHSOCode*)transformDataIntoClass:(NSData*)data;
- (void)separeCodeIntoPages;
-(NSData*)generateCodeToStore;
- (void) storeInDefaults;
- (void) removeFromDefaults;
- (NSArray*)getOcuppiedIndexPages;
- (void)eraseMemoryPosition;
- (int)getProgramCounterInCurrentPage;
- (int)getCurrentPage;
- (void)adjustPCInPage;
- (void)resetSettings;
@end
