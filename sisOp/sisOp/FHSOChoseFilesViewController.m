//
//  FHSOChoseFilesViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 4/2/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOChoseFilesViewController.h"
#import "FHSOCode.h"
#import "FHSOMemory.h"
#import "FHSOProcessSettingController.h"

@interface FHSOChoseFilesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIStepper *numberOfPagesStepper;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfPages;
@property (weak, nonatomic) IBOutlet UIStepper *pageSizeStepper;
@property (weak, nonatomic) IBOutlet UILabel *lblPageSize;

@end

@implementation FHSOChoseFilesViewController
{
    NSMutableArray *codeList;
    NSMutableArray *codeExecuteList;
    NSMutableArray *listFromDefaults;
    FHSOCode *currentCode;
    FHSOMemory *memory;
    BOOL allFiles;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}

- (void)configureView
{
    allFiles = YES;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.navigationController.navigationBarHidden = YES;
    listFromDefaults = [[NSMutableArray alloc] init];
    codeList = [[NSMutableArray alloc] init];
    codeExecuteList = [[NSMutableArray alloc] init];
    [self initMemoryList];
    [self configureMemory];
}

- (void)configureMemory
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"memory"];
    if (data == nil)
    {
        memory = [[FHSOMemory alloc] init];
        self.numberOfPagesStepper.enabled = YES;
        self.pageSizeStepper.enabled = YES;
    }
    else
    {
        memory = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        self.numberOfPagesStepper.enabled = NO;
        self.pageSizeStepper.enabled = NO;
        self.lblNumberOfPages.text = [NSString stringWithFormat:@"%d", memory.numberOfPages];
        self.lblPageSize.text = [NSString stringWithFormat:@"%d", (int)memory.pageLength/2];
    }
    
}
- (IBAction)changeValue:(UIStepper *)sender {
    if (sender == self.numberOfPagesStepper)
    {
        self.lblNumberOfPages.text = [NSString stringWithFormat:@"%d", (int)sender.value];
    }
    else
    {
        self.lblPageSize.text = [NSString stringWithFormat:@"%d", (int)sender.value];
    }
}

- (void) initMemoryList
{
    listFromDefaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"codeList"];
    for (NSData *data in listFromDefaults)
    {
        FHSOCode *myCode = [FHSOCode transformDataIntoClass:data];
        [codeList addObject:myCode];
        if (myCode.willExecute)
        {
            [codeExecuteList addObject: myCode];
        }
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (IBAction)backToMenu:(id)sender {
    if (self.numberOfPagesStepper.enabled)
    {
        memory.pageLength = self.pageSizeStepper.value * 2;
        [memory setNumberOfPages:self.numberOfPagesStepper.value];
        if (memory.pageLength > 0 && memory.numberOfPages > 0)
        [memory storeInDefaults];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        FHSOCode *code = [codeList objectAtIndex:indexPath.row];
        [code removeFromDefaults];
        [codeList removeObject:code];
        [codeExecuteList removeObject:code];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}

- (IBAction)sgtControl:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0)
        allFiles = YES;
    else
        allFiles = NO;
    [_tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Archives";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (allFiles)
        return codeList.count;
    else
        return codeExecuteList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *memoryPosition = (UILabel *) [cell viewWithTag:1];
    memoryPosition.textColor = [UIColor blackColor];
    
    if (allFiles)
    {
        if (((FHSOCode*)codeList[indexPath.row]).willExecute)
        {
            UIImageView *imageView = (UIImageView*) [cell viewWithTag:3];
            imageView.image = [UIImage imageNamed:@"processExecute"];
        }
        memoryPosition.text = ((FHSOCode*)codeList[indexPath.row]).name;
    }
    else
    {
        memoryPosition.text = ((FHSOCode*)codeExecuteList[indexPath.row]).name;
        UIImageView *imageView = (UIImageView*) [cell viewWithTag:3];
        imageView.image = nil;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"processSettings" sender:indexPath];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSIndexPath*)sender
{
    if ([segue.identifier isEqualToString:@"processSettings"])
    {
        FHSOProcessSettingController *view = (FHSOProcessSettingController*) segue.destinationViewController;
        view.codeProcess = codeList[sender.row];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self configureView];
    [_tableView reloadData];
}



@end
