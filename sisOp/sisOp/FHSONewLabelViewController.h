//
//  FHSONewLabelViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 5/13/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSOCode.h"

@interface FHSONewLabelViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic) FHSOCode *code;

@end
