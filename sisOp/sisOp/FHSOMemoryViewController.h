//
//  FHSOMemoryViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/7/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSOMemoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) NSMutableArray *memoryList;
@property (nonatomic) NSMutableArray *memoryLabelsList;
@property (nonatomic) int currentOperation; 

@end
