//
//  FHSOInstruction.h
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHSOInstruction : NSObject
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *category;
@property (nonatomic) NSString *description;
@property (nonatomic) NSString *function;

- (instancetype)initWithName:(NSString *)aName andDescription:(NSString *)aDescription andCategory:(NSString *)aCategory andFunction:(NSString *)aFunction;

@end
