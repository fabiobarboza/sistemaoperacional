//
//  FHSOCode.m
//  sisOp
//
//  Created by Fabio Barboza on 3/14/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOCode.h"
#import "FHSOIndexToMemory.h"

@implementation FHSOCode
{
    NSMutableArray *wordsFromText;
}

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        _instructionList = [[NSMutableArray alloc] init];
        _dataList = [[NSMutableArray alloc] init];
        _labelList = [[NSMutableArray alloc] init];
        _name = [[NSString alloc] init];
        _pagesList = [[NSMutableArray alloc] init];
        _description = [[NSString alloc] init];
    }
    return self;
}

//PAGESLIST SERÁ UMA MATRIZ, NA POSIÇÃO [I][0] VAI TER O ENDERECO DO FRAME NA MEMÓRIA PRINCIPAL, NA OUTRA PARTE, UM VETOR COM AS INSTRUÇOES;
//USUARIO TERÁ DE SETAR OS VALORES DO TAMANHO DA PAGE LIST E NA HORA DE EXECUTAR, CHAMAR O MÉTODO PARA GERAR A SEPARAÇAO

- (instancetype) initWithInstruction:(NSMutableArray*)instruction
                            withData:(NSMutableArray*)data
                           withLabel:(NSMutableArray*)label
                            withName:(NSString*)objectName
                     withDescription:(NSString*)objectDescription
                     withArrivalTime:(int)arrivalTimer
                  withProgramCounter:(int)programCounter
                      withAcumulator:(int)acumulator
                       withRemainder:(int)remainder
                         withExecute:(int)execute
                        withPageList:(NSMutableArray*)pageList
{
    self = [super init];
    if (self)
    {
        _instructionList = [[NSMutableArray alloc] initWithArray: instruction];
        _dataList = [[NSMutableArray alloc] initWithArray:data];
        _labelList = [[NSMutableArray alloc] initWithArray:label];
        _name = [[NSString alloc] initWithString:objectName];
        _description = [[NSString alloc] initWithString:objectDescription];
        _pagesList = [[NSMutableArray alloc] initWithArray:pageList];
        _acumulator = acumulator;
        _arrivalTime = arrivalTimer;
        _processTime = (int)_instructionList.count/2;
        _programCounter = programCounter;
        _executeTime = execute;
        if (remainder == 0)
            _remainTime = _processTime; 
            else
                _remainTime = remainder;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_instructionList forKey:@"instructionList"];
    [encoder encodeObject:_dataList forKey:@"dataList"];
    [encoder encodeObject:_labelList forKey:@"labelList"];
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeObject:_description forKey:@"description"];
    [encoder encodeBool:_isCurrent forKey:@"isCurrent"];
    [encoder encodeInt:_arrivalTime forKey:@"arrivalTime"];
    [encoder encodeInt:_programCounter forKey:@"programCounter"];
    [encoder encodeInt:_acumulator forKey:@"acumulator"];
    [encoder encodeBool:_willExecute forKey:@"execute"];
    [encoder encodeInt:_remainTime forKey:@"remainderProcessTimer"];
    [encoder encodeInt:_lastTime forKey:@"lastTime"];
    [encoder encodeInt:_waitingTime forKey:@"waitingTime"];
    [encoder encodeInt:_executeTime forKey:@"executeTime"];
    [encoder encodeInt:_priority forKey:@"priority"];
    [encoder encodeObject:_pagesList forKey:@"pageList"];
    [encoder encodeInt:_pageLength forKey:@"pageLength"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    NSMutableArray *instruction = [[NSMutableArray alloc] initWithArray:[decoder decodeObjectForKey:@"instructionList"]];
    NSMutableArray *data = [[NSMutableArray alloc] initWithArray:[decoder decodeObjectForKey:@"dataList"]];
    NSMutableArray *label = [[NSMutableArray alloc] initWithArray:[decoder decodeObjectForKey:@"labelList"]];
    NSMutableArray *page = [[NSMutableArray alloc] initWithArray:[decoder decodeObjectForKey:@"pageList"]];
    NSString *objectName = [[NSString alloc] initWithString: [decoder decodeObjectForKey:@"name"]];
    _isCurrent = [decoder decodeBoolForKey:@"isCurrent"];
    int arrivalTimer = [decoder decodeIntForKey:@"arrivalTime"];
    int programCounter = [decoder decodeIntForKey:@"programCounter"];
    int acumulator = [decoder decodeIntForKey:@"acumulator"];
    _willExecute = [decoder decodeBoolForKey:@"execute"];
    int remainder = [decoder decodeIntForKey:@"remainderProcessTimer"];
    _lastTime = [decoder decodeIntForKey:@"lastTime"];
    _waitingTime = [decoder decodeIntForKey:@"waitingTime"];
    int executeTime = [decoder decodeIntForKey:@"executeTime"];
    _priority = [decoder decodeIntForKey:@"priority"];
    _pageLength = [decoder decodeIntForKey:@"pageLength"];
    NSString *objectDescription = [decoder decodeObjectForKey:@"description"];
    return [self initWithInstruction:instruction withData:data withLabel:label withName:objectName withDescription:objectDescription withArrivalTime:arrivalTimer withProgramCounter:programCounter withAcumulator:acumulator withRemainder:remainder withExecute:executeTime withPageList:page];
}



+ (FHSOCode*) transformDataIntoClass:(NSData*)data
{
    FHSOCode *code = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return code;
}


- (NSData*)generateCodeToStore
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    return data;
}

- (void)storeInDefaults
{
    NSArray *codeList = [[NSUserDefaults standardUserDefaults] objectForKey:@"codeList"];
    NSMutableArray *newCodeList = [[NSMutableArray alloc] init];
    for (NSData *data in codeList)
    {
        if (![((FHSOCode*)[FHSOCode transformDataIntoClass:data]).name isEqualToString:_name])
        {
            [newCodeList addObject: data];
        }
            
    }
    [newCodeList addObject: [self generateCodeToStore]];
    [[NSUserDefaults standardUserDefaults] setObject:newCodeList forKey:@"codeList"]; 
}

- (void)separeCodeIntoPages
{
    self.pagesList = [[NSMutableArray alloc] init]; 
    NSMutableArray *instructionToPage = [[NSMutableArray alloc] init];
    NSMutableArray *pageAndInstruction = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.instructionList.count;)
    {
        instructionToPage = [[NSMutableArray alloc] init];
        pageAndInstruction = [[NSMutableArray alloc] init]; 
        for (int j = 0; j < self.pageLength; j++)
        {
            if (i < self.instructionList.count)
            {
                [instructionToPage addObject:self.instructionList[i]];
                i++;
            }
        }
        FHSOIndexToMemory *indexToMemory = [[FHSOIndexToMemory alloc] init];
        indexToMemory.pageNumber = 0;
        indexToMemory.programCounterInPage = 0;
        indexToMemory.processName = self.name;
        [pageAndInstruction addObject:indexToMemory];
        [pageAndInstruction addObject:instructionToPage];
        [self.pagesList addObject:pageAndInstruction];
    }
    
//    NSMutableArray *dataToPage = [[NSMutableArray alloc] init];
//    NSMutableArray *pageAndData = [[NSMutableArray alloc] init];
//    
//    for (int i = 0; i < self.dataList.count;)
//    {
//        dataToPage = [[NSMutableArray alloc] init];
//        pageAndData = [[NSMutableArray alloc] init];
//        for (int j = 0; j < self.pageLength; j++)
//        {
//            if (i < self.dataList.count)
//            {
//                [dataToPage addObject:self.dataList[i]];
//                i++;
//            }
//        }
//        FHSOIndexToMemory *indexToMemory = [[FHSOIndexToMemory alloc] init];
//        indexToMemory.pageNumber = 0;
//        indexToMemory.programCounterInPage = 0;
//        indexToMemory.processName = self.name;
//        [pageAndData addObject:indexToMemory];
//        [pageAndData addObject:dataToPage];
//        [self.pagesList addObject:pageAndData];
//    }
    
}

- (void)removeFromDefaults
{
    NSArray *codeList = [[NSUserDefaults standardUserDefaults] objectForKey:@"codeList"];
    NSMutableArray *newCodeList = [[NSMutableArray alloc] init];
    for (NSData *data in codeList)
    {
        if (![((FHSOCode*)[FHSOCode transformDataIntoClass:data]).name isEqualToString:_name])
        {
            [newCodeList addObject: data];
        }
        
    }
    [[NSUserDefaults standardUserDefaults] setObject:newCodeList forKey:@"codeList"];
}

- (NSArray*)getOcuppiedIndexPages
{
    NSMutableArray *listWithIndex = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.pagesList.count; i++)
    {
        [listWithIndex addObject:self.pagesList[i][0]];
    }
    return listWithIndex;
}

- (void)eraseMemoryPosition
{
    FHSOIndexToMemory *indexToMemory;
    for (int i = 0; i < self.pagesList.count; i++)
    {
        indexToMemory = self.pagesList[i][0];
        indexToMemory.pageNumber = 0;
    }
}

- (int)getProgramCounterInCurrentPage
{
    int numberOfCurrentPage = (int)self.programCounter/self.pageLength;
    return self.programCounter - (numberOfCurrentPage * self.pageLength);
}

- (void)adjustPCInPage
{
    if ((int)self.programCounter/self.pageLength < self.pagesList.count)
    {
        FHSOIndexToMemory *index = self.pagesList[(int)self.programCounter/self.pageLength][0];
        index.programCounterInPage = [self getProgramCounterInCurrentPage];
    }
}

- (int)getCurrentPage
{
    return (int)self.programCounter/self.pageLength;
}

- (void)resetSettings
{
    self.willExecute = NO;
    self.isCurrent = NO;
    self.programCounter = 0;
    self.arrivalTime = 0;
    self.waitingTime = 0;
    self.remainTime = 0; 
    self.executeTime = 0;
}

@end
