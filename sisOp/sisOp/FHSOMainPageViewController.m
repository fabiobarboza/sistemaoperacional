//
//  FHSOMainPageViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/7/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOMainPageViewController.h"
#import "FHSOCode.h"
#import "FHSOSjF.h"
#import "FHSOExecutionDetailViewController.h"
#import "FHSORoundRobin.h"

@interface FHSOMainPageViewController ()
@property (strong, nonatomic) IBOutlet UILabel *currentOperation;
@property (strong, nonatomic) IBOutlet UILabel *nextOperation;
@property (strong, nonatomic) IBOutlet UIButton *btnPlay;
@property (strong, nonatomic) IBOutlet UIButton *btnFoward;
@property (weak, nonatomic) IBOutlet UILabel *pcLabel;
@property (weak, nonatomic) IBOutlet UILabel *acLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblProcess;
@property (weak, nonatomic) IBOutlet UILabel *lblPage;

@end

@implementation FHSOMainPageViewController
{
    NSMutableArray *memoryList;
    NSMutableArray *memoryData;
    NSMutableArray *memoryLabels;
    NSArray *memoryInstructionsAndVariables;
    NSMutableArray *memoryDataFromUserDefaults;
    NSArray *memoryLabelsFromUserDefaults;
    NSTimer *myTimer;
    int currentLine;
    BOOL isSjs;
    FHSOCode *code;
    FHSORoundRobin *roundRobin;
    FHSOSjF *sjs;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}

- (void)configureView
{
    code = [[FHSOCode alloc] init];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"memory"];
    if (data != nil)
    {
        self.memory = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if (self.memory.pageLength > 0 && self.memory.numberOfPages > 0)
        {
            NSString *valueFromSjs = [[NSUserDefaults standardUserDefaults] objectForKey:@"sjs"];
            if (valueFromSjs != nil)
            {
                if ([valueFromSjs isEqualToString:@"YES"])
                {
                    isSjs = YES;
                    sjs = [[FHSOSjF alloc] init];
                    sjs.totalTime = [[[NSUserDefaults standardUserDefaults] objectForKey:@"totalTime"] floatValue];
                    sjs.memory = self.memory;
                    [sjs startSJS];
                    NSLog(@"Method: SJS");
                }
                else
                {
                    isSjs = NO;
                    roundRobin = [[FHSORoundRobin alloc] init];
                    roundRobin.totalTime = [[[NSUserDefaults standardUserDefaults] objectForKey:@"totalTime"] floatValue];
                    [roundRobin startRoundRobin];
                    roundRobin.memory = self.memory;
                    NSLog(@"Method: RR");
                }
            }

        }
    }    
    [self.btnMenu addTarget:self action:@selector(backToMenu) forControlEvents:UIControlEventTouchUpInside];
}

- (void)backToMenu
{
    if (isSjs)
    {
        [sjs saveExecuteListInDefaults];
        if (sjs.finished)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc] initWithFloat:0] forKey:@"totalTime"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"statisctsList"];
        }
        else
            [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc] initWithFloat:sjs.totalTime] forKey:@"totalTime"];
    }
    else
    {
        [roundRobin saveExecuteListInDefaults];
        if (roundRobin.finished)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc] initWithFloat:0] forKey:@"totalTime"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"statisctsList"];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:[[NSNumber alloc] initWithFloat:roundRobin.totalTime] forKey:@"totalTime"];
        }
        
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"memory"] != nil)
        [self.memory storeInDefaults];
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) initMemoryListWithAllInformationsFromInstructionsAndData
{
    NSArray *codeList = [[NSUserDefaults standardUserDefaults] objectForKey:@"codeList"];
    for (NSData *data in codeList)
    {
        FHSOCode *currentCode = [[FHSOCode alloc] init];
        currentCode = [FHSOCode transformDataIntoClass:data];
        if (currentCode.isCurrent)
            code = currentCode;
    }
}


- (IBAction)playAndStop:(UIButton *)sender {
    if (sender.tag == 1)
    {
        [sender setBackgroundImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
        sender.tag = 2;
        myTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                   target:self
                                                 selector:@selector(stepByStep:)
                                                 userInfo:nil
                                                  repeats:YES];
        _btnFoward.enabled = NO;
    }
    else if (sender.tag == 2)
    {
        [sender setBackgroundImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        sender.tag = 1;
        [myTimer invalidate];
        myTimer = nil;
        _btnFoward.enabled = YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [myTimer invalidate];
    myTimer = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
    [self initMemoryListWithAllInformationsFromInstructionsAndData];
}


- (IBAction)stepByStep:(UIButton *)sender {
    
    if (isSjs)
        code = [sjs checkSJS];
    else
        code = [roundRobin checkRoundRobin];
    
    if (code != nil)
    {
        currentLine = code.programCounter;
        
        [self configureLabelByCode];
        
        if (currentLine < code.instructionList.count)
        {
            [self configureMemory];
            
            [self executeOperation];
            
            [self adjustCodeSettings];
            
            if (isSjs)
            {
                [self adjustSjs];
            }
            else
            {
                [self adjustRR];
            }
        }
    }
    else
    {
        [self performSegueWithIdentifier:@"executionDetailSegue" sender:nil];
    }
}

- (void)configureLabelByCode
{
    _lblProcess.text = code.name;
    _lblPage.text = [NSString stringWithFormat:@"%d", [code getCurrentPage]];
    _acLabel.text = [NSString stringWithFormat:@"%d", code.acumulator];
    _pcLabel.text = [NSString stringWithFormat:@"%d", code.programCounter];
}

- (void)configureOperation
{
    NSString *firstOperation = code.instructionList[currentLine];
    NSString *secondOperation = code.instructionList[currentLine+1];
    NSString *lineOperation = firstOperation;
    lineOperation = [lineOperation stringByAppendingString:@" "];
    lineOperation = [lineOperation stringByAppendingString: secondOperation];
}

- (void)configureMemory
{
    if (self.memory.lastPage < [code getCurrentPage])
    {
        if ([self.memory.memory[([code getCurrentPage]-1)] isKindOfClass:[NSArray class]])
        {
            ((FHSOIndexToMemory *)self.memory.memory[([code getCurrentPage]-1)][0]).programCounterInPage = 0;
            self.memory.lastPage = [code getCurrentPage];
            self.memory.totalPageCount++;
            [self.memory storeInDefaults];
        }
    }
    if (self.memory.totalPageCount == self.memory.memory.count)
    {
        if ([self.memory.memory[self.memory.totalPageCount-1] isKindOfClass:[NSArray class]])
            ((FHSOIndexToMemory *)self.memory.memory[self.memory.totalPageCount-1][0]).programCounterInPage = [code getProgramCounterInCurrentPage];
    }
    else
    {
        if ([self.memory.memory[self.memory.totalPageCount] isKindOfClass:[NSArray class]])
            ((FHSOIndexToMemory *)self.memory.memory[self.memory.totalPageCount][0]).programCounterInPage = [code getProgramCounterInCurrentPage];
    }
}

- (void)adjustCodeSettings
{
    code.programCounter = currentLine;
    code.acumulator = [self.acLabel.text intValue];
    code.executeTime++;
}

- (void)adjustSjs
{
    sjs.processExecutionTime++;
    sjs.totalTime++;
    code.remainTime--;
}

- (void)adjustRR
{
    roundRobin.processExecutionTime++;
    roundRobin.totalTime++;
}

- (void)executeOperation
{
    NSString *firstOperation = code.instructionList[currentLine];
    NSString *secondOperation = code.instructionList[currentLine+1];
    NSString *lineOperation = firstOperation;
    lineOperation = [lineOperation stringByAppendingString:@" "];
    lineOperation = [lineOperation stringByAppendingString: secondOperation];
    
    
    _currentOperation.text = lineOperation;
    
    _pcLabel.text = [NSString stringWithFormat:@"%@", [self binaryStringFromInteger:currentLine]];
    
    currentLine += 2;
    if (currentLine < code.instructionList.count)
    {
        lineOperation = code.instructionList[currentLine];
        lineOperation = [lineOperation stringByAppendingString:@" "];
        lineOperation = [lineOperation stringByAppendingString: code.instructionList[currentLine+1]];
        _nextOperation.text = lineOperation;
    }
    else
    {
        _nextOperation.text = @"Empty";
    }
    
    
    if ([firstOperation isEqualToString:@"add"] || [firstOperation isEqualToString:@"ADD"])
    {
        int valueFromAcumulator = [_acLabel.text intValue];
        if ([secondOperation characterAtIndex:0] == '#')
        {
            NSString *valueFromChar = [NSString stringWithFormat:@"%c", [secondOperation characterAtIndex:1]];
            valueFromAcumulator += [valueFromChar intValue];
        }
        else
        {
            valueFromAcumulator += [self getValueFromData:secondOperation];
        }
        _acLabel.text = [NSString stringWithFormat:@"%d", valueFromAcumulator];
    }
    else if ([firstOperation isEqualToString:@"SUB"] || [firstOperation isEqualToString:@"sub"])
    {
        int valueFromAcumulator = [_acLabel.text intValue];
        if ([secondOperation characterAtIndex:0] == '#')
        {
            
            NSString *valueFromChar = [NSString stringWithFormat:@"%c", [secondOperation characterAtIndex:1]];
            valueFromAcumulator -= [valueFromChar intValue];
        }
        else
        {
            valueFromAcumulator -= [self getValueFromData:secondOperation];
        }
        _acLabel.text = [NSString stringWithFormat:@"%d", valueFromAcumulator];
    }
    else if ([firstOperation isEqualToString:@"mult"] || [firstOperation isEqualToString:@"MULT"])
    {
        int valueFromAcumulator = [_acLabel.text intValue];
        if ([secondOperation characterAtIndex:0] == '#')
        {
            NSString *valueFromChar = [NSString stringWithFormat:@"%c", [secondOperation characterAtIndex:1]];
            valueFromAcumulator *= [valueFromChar intValue];
        }
        else
        {
            valueFromAcumulator *= [self getValueFromData:secondOperation];
        }
        _acLabel.text = [NSString stringWithFormat:@"%d", valueFromAcumulator];
    }
    else if ([firstOperation isEqualToString:@"div"] || [firstOperation isEqualToString:@"DIV"])
    {
        int valueFromAcumulator = [_acLabel.text intValue];
        if ([secondOperation characterAtIndex:0] == '#')
        {
            NSString *valueFromChar = [NSString stringWithFormat:@"%c", [secondOperation characterAtIndex:1]];
            valueFromAcumulator /= [valueFromChar intValue];
        }
        else
        {
            valueFromAcumulator /= [self getValueFromData:secondOperation];
        }
        _acLabel.text = [NSString stringWithFormat:@"%d", valueFromAcumulator];
    }
    else if ([firstOperation isEqualToString:@"load"] || [firstOperation isEqualToString:@"LOAD"])
    {
        _acLabel.text = [NSString stringWithFormat:@"%d", [self getValueFromData:secondOperation]];
        
    }
    else if ([firstOperation isEqualToString:@"store"] || [firstOperation isEqualToString:@"STORE"])
    {
        [self storeValue:[_acLabel.text intValue] inData:secondOperation];
    }
    else if ([firstOperation isEqualToString:@"brany"] || [firstOperation isEqualToString:@"BRANY"])
    {
        NSString *position = [self getPositionFromLabel:secondOperation];
        currentLine = position.intValue +1;
    }
    else if ([firstOperation isEqualToString:@"brpos"] || [firstOperation isEqualToString:@"BRPOS"])
    {
        if(_acLabel.text.intValue > 0)
        {
            NSString *position = [self getPositionFromLabel:secondOperation];
            currentLine = position.intValue +1;
        }
    }
    else if ([firstOperation isEqualToString:@"brzero"] || [firstOperation isEqualToString:@"BRZERO"])
    {
        if(_acLabel.text.intValue == 0)
        {
            NSString *position = [self getPositionFromLabel:secondOperation];
            currentLine = position.intValue +1;
        }
    }
    else if ([firstOperation isEqualToString:@"brneg"] || [firstOperation isEqualToString:@"BRNEG"])
    {
        if(_acLabel.text.intValue < 0)
        {
            NSString *position = [self getPositionFromLabel:secondOperation];
            currentLine = position.intValue +1;
        }
    }
    else if ([firstOperation isEqualToString:@"SYSCALL"])
    {
        
    }
    
}

-(NSString * )binaryStringFromInteger:(int) aNumber
{
    NSMutableString * string = [[NSMutableString alloc] init];
    
    int spacing = pow(2, 3);
    int width = ( sizeof( aNumber ) ) * spacing;
    int binaryDigit = 0;
    int integer = aNumber;
    
    while( binaryDigit < width )
    {
        binaryDigit++;
        
        [string insertString:( (integer & 1) ? @"1" : @"0" )atIndex:0];
        
        if( binaryDigit % spacing == 0 && binaryDigit != width )
        {
            [string insertString:@" " atIndex:0];
        }
        
        integer = integer >> 1;
    }
    
    NSArray *myWords = [string componentsSeparatedByString:@" "];
    
    
    return myWords[myWords.count -1];
}

- (NSString *) getPositionFromLabel:(NSString *)aLabel
{
    for (int i = 0; i < code.labelList.count -1; i+=2)
    {
        if ([code.labelList[i] isEqualToString:aLabel])
        {
            return (NSString *)code.labelList[i+1];
        }
    }
    return nil;
}

- (void) storeValue : (int) value
             inData : (NSString*) nameData
{
    for (int i = 0; i < code.dataList.count-1; i+=2)
    {
        if ([code.dataList[i] isEqualToString: nameData])
        {
            code.dataList[i + 1] = [NSString stringWithFormat:@"%d", value];
            [code storeInDefaults];
        }
        
    }
}
- (int) getValueFromData : (NSString *) nameData
{
    for (int i = 0; i < code.dataList.count-1; i+=2)
    {
        if ([code.dataList[i] isEqualToString: nameData])
        {
            NSString* stringFromData = code.dataList[i + 1];
            return [stringFromData intValue];
        }
    }
    return 0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    FHSOExecutionDetailViewController *controller = (FHSOExecutionDetailViewController *)segue.destinationViewController;
    if (isSjs)
        controller.codeStatisctsList = sjs.codeStatisctsList;
    else
        controller.codeStatisctsList = roundRobin.codeStatisctsList;
}

@end
