//
//  FHSOInfoViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 4/3/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOInfoViewController.h"

@interface FHSOInfoViewController ()
@property (nonatomic) UIView *tutorial;

@end

@implementation FHSOInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)menuClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */


- (void)createTutorial
{
    self.tutorial = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tutorial.backgroundColor = [UIColor clearColor];
    
    UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    grayView.backgroundColor = [UIColor blackColor];
    grayView.alpha = .8;
    [self.tutorial addSubview:grayView];
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    scroll.pagingEnabled = YES;
    scroll.contentSize = CGSizeMake(320 * 6, 568);
    scroll.delegate = self;
    
    for (int i = 0; i < 6; i++)
    {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(46 + (320 * i), 43, 229, 482)];
        img.image = [UIImage imageNamed:[NSString stringWithFormat:@"tutorial%d.png",i+1]];
        [scroll addSubview:img];
    }
    [self.tutorial addSubview:scroll];
    
    UIPageControl* pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(85, 511, 150, 37)];
    pageControl.center = CGPointMake(190, 540);
    pageControl.tag = 30;
    pageControl.numberOfPages = 6;
    pageControl.currentPage = 0;
    [pageControl sizeToFit];
    
    [self.tutorial addSubview:pageControl];
    
    UILabel *lblWelcome = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 80)];
    lblWelcome.textAlignment = NSTextAlignmentCenter;
    lblWelcome.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:30];
    lblWelcome.text = @"Welcome to Origami OS!";
    lblWelcome.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:.95];
    lblWelcome.textColor = [UIColor blackColor];
    [self.tutorial addSubview:lblWelcome];
    
    UIButton *close = [[UIButton alloc] initWithFrame:CGRectMake(220, 532, 95, 30)];
    close.backgroundColor = [UIColor colorWithRed:222./255. green:54./255. blue:15./255. alpha:1];
    [close setTitle:@"Skip Tutorial" forState:UIControlStateNormal];
    [close setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    close.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:18];
    close.layer.borderWidth = 1.;
    close.layer.borderColor = [UIColor clearColor].CGColor;
    close.layer.cornerRadius = 5;
    [close addTarget:self action:@selector(closeTutorial) forControlEvents:UIControlEventTouchUpInside];
    [self.tutorial addSubview:close];
    
    self.tutorial.alpha = 0;
    [self.view addSubview:self.tutorial];
    [UIView animateWithDuration:.4 animations:^{
        self.tutorial.alpha = 1;
    }completion:^(BOOL finished) {
        
    }];
}

- (void)closeTutorial
{
    [UIView animateWithDuration:.4 animations:^{
        self.tutorial.alpha = 0;
    }completion:^(BOOL finished) {
        self.tutorial.hidden = YES;
    }];
}

- (IBAction)showTutorial:(UIButton *)sender {
    if (!self.tutorial)
        [self createTutorial];
    else
    {
        self.tutorial.hidden = NO;
        [UIView animateWithDuration:.4 animations:^{
            self.tutorial.alpha = 1;
        }completion:^(BOOL finished) {
            
        }];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x/ scrollView.frame.size.width;
    UIPageControl *pageControl = (UIPageControl *)[self.tutorial viewWithTag:30];
    pageControl.currentPage = page;
}


@end
