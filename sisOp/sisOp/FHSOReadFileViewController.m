//
//  FHSOReadFileViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/7/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOReadFileViewController.h"
#import "FHSOReader.h"
#import "FHSONewCodeSelectorViewController.h"
#import "FHSONewCodeViewController.h"
#import "FHSONewDataViewController.h"


@interface FHSOReadFileViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FHSOReadFileViewController
{
    FHSOReader *textReader;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
        return _code.instructionList.count/2;
    return _code.dataList.count/2;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    if (section == 0)
        return @".endcode";
    
    return @".enddata";
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return @".code";
    
    return @".data";
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
        [self performSegueWithIdentifier:@"editInstruction" sender:indexPath];
    else
        [self performSegueWithIdentifier:@"editData" sender:indexPath]; 
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    int valueRowForInstructions = (int)indexPath.row * 2;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *memoryPosition = (UILabel *) [cell viewWithTag:1];
    memoryPosition.textColor = [UIColor blackColor];
    if (indexPath.section == 0)
    {
        NSString *instructionWithVariable = [_code.instructionList objectAtIndex: valueRowForInstructions];
        instructionWithVariable = [instructionWithVariable stringByAppendingString: @" "];
        instructionWithVariable = [instructionWithVariable stringByAppendingString: [_code.instructionList objectAtIndex: valueRowForInstructions +1]];
        memoryPosition.text = instructionWithVariable;
    }
    else
    {
        int valueRowForData = (int)indexPath.row * 2;
        NSString *variableWithValue = [_code.dataList objectAtIndex: valueRowForData];
        variableWithValue = [variableWithValue stringByAppendingString: @" "];
        variableWithValue = [variableWithValue stringByAppendingString: [_code.dataList objectAtIndex: valueRowForData +1]];
        memoryPosition.text = variableWithValue;
    }
        return cell;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void) viewWillAppear:(BOOL)animated
{
    textReader = [[FHSOReader alloc] init];
    [_tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"newCodeSegue"])
    {
        FHSONewCodeSelectorViewController *destinatioVC = segue.destinationViewController;
        destinatioVC.code = _code;
    }
    else if ([segue.identifier isEqualToString:@"editInstruction"])
    {
        FHSONewCodeViewController *codeController = (FHSONewCodeViewController*) segue.destinationViewController;
        codeController.code = _code;
        codeController.isEditing = YES; 
        codeController.operationIndexInInstructionList = (int)((NSIndexPath*)sender).row;
    }
    else if ([segue.identifier isEqualToString:@"editData"])
    {
        FHSONewDataViewController *dataController = (FHSONewDataViewController*) segue.destinationViewController;
        dataController.code = _code;
        dataController.isEditing = YES;
        dataController.dataIndexAtDataList = (int)((NSIndexPath*)sender).row;
    }
 
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_code.instructionList removeObjectAtIndex:indexPath.row];
        [_code.instructionList removeObjectAtIndex:indexPath.row];
        [_code storeInDefaults];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
