//
//  FHSOExecutionDetailViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 5/10/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSOExecutionDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSMutableArray *codeStatisctsList;
@end
