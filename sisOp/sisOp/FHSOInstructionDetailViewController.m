//
//  FHSOInstructionDetailViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOInstructionDetailViewController.h"

@interface FHSOInstructionDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblFunction;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;

@end

@implementation FHSOInstructionDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self configureView];
}

- (void)configureView
{
    _lblName.text = _currentInstruction.name;
    _lblCategory.text = _currentInstruction.category;
    _lblFunction.text = _currentInstruction.function;
    _txtDescription.text = _currentInstruction.description;
    _txtDescription.editable = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
