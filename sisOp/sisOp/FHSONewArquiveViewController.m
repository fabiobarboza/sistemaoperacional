//
//  FHSONewArquiveViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/18/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSONewArquiveViewController.h"
#import "FHSOCode.h"

@interface FHSONewArquiveViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end

@implementation FHSONewArquiveViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnConfirm:(UIButton *)sender {
    FHSOCode *code = [[FHSOCode alloc] init];
    code.name = _txtName.text;
    code.description = _txtDescription.text;
    code.priority = self.segmentedControl.selectedSegmentIndex; 
    [code storeInDefaults];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - keyboard movements

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtDescription resignFirstResponder];
    [self.txtName resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -45.0f;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = +0.0f;
        self.view.frame = f;
    }];
}

@end
