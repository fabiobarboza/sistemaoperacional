//
//  FHSOSjF.h
//  sisOp
//
//  Created by Fabio Barboza on 4/3/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FHSOCode.h"
#import "FHSOMemory.h"

@interface FHSOSjF : NSObject
@property (nonatomic) FHSOMemory *memory;
@property (nonatomic) NSMutableArray *codeList; 
@property (nonatomic) NSMutableArray *codeExecuteList;
@property (nonatomic) NSMutableArray *suspendedList;
@property (nonatomic) NSMutableArray *codeStatisctsList; 
@property (nonatomic) int totalTime;
@property (nonatomic) int processExecutionTime;
@property (nonatomic) BOOL finished;
- (FHSOCode*)checkSJS;
- (void)startSJS;
- (void)saveExecuteListInDefaults;
@end
