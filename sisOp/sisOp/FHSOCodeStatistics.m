//
//  FHSOCodeStatistics.m
//  sisOp
//
//  Created by Bruno Vieira Bulso on 5/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOCodeStatistics.h"

@implementation FHSOCodeStatistics

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.description forKey:@"description"];
    [aCoder encodeInt:self.arrivalTime forKey:@"arrival"];
    [aCoder encodeInt:self.processTime forKey:@"process"];
    [aCoder encodeInt:self.executeTime forKey:@"execute"];
    [aCoder encodeInt:self.waitingTime forKey:@"waiting"];
    [aCoder encodeInt:self.acumulator forKey:@"acumulator"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.description = [aDecoder decodeObjectForKey:@"description"];
        self.arrivalTime = [aDecoder decodeIntForKey:@"arrival"];
        self.processTime = [aDecoder decodeIntForKey:@"process"];
        self.executeTime = [aDecoder decodeIntForKey:@"execute"];
        self.waitingTime = [aDecoder decodeIntForKey:@"waiting"];
        self.acumulator = [aDecoder decodeIntForKey:@"acumulator"];
    }
    return self;
}

@end
