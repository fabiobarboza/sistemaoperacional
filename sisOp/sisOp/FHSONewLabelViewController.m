//
//  FHSONewLabelViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 5/13/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSONewLabelViewController.h"

@interface FHSONewLabelViewController ()
@property (weak, nonatomic) IBOutlet UITextField *lblName;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@end

@implementation FHSONewLabelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configurePickerView];
}

- (void)configurePickerView
{
    self.picker.dataSource = self;
    self.picker.delegate = self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.lblName resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)confirmAction:(UIButton *)sender
{
    [_code.labelList addObject:self.lblName.text];
    [_code.labelList addObject:[[NSNumber alloc] initWithInt:(int)[self.picker selectedRowInComponent:0]]];
    [_code storeInDefaults]; 
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@ %@",self.code.instructionList[row *2], self.code.instructionList[(row *2)+1]];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.code.instructionList.count/2;
}

@end
