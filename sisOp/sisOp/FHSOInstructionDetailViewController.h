//
//  FHSOInstructionDetailViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSOInstruction.h"

@interface FHSOInstructionDetailViewController : UIViewController
@property (nonatomic) FHSOInstruction *currentInstruction;

@end
