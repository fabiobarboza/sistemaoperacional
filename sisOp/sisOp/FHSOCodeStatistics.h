//
//  FHSOCodeStatistics.h
//  sisOp
//
//  Created by Bruno Vieira Bulso on 5/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHSOCodeStatistics : NSObject<NSCoding>
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *description;
@property (nonatomic) int arrivalTime;
@property (nonatomic) int processTime;
@property (nonatomic) int executeTime;
@property (nonatomic) int waitingTime;
@property (nonatomic) int acumulator;
@end
