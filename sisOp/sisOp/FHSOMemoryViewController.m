//
//  FHSOMemoryViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/7/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOMemoryViewController.h"
#import "FHSOCode.h"

@interface FHSOMemoryViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblProcess;

@end

@implementation FHSOMemoryViewController



- (IBAction)backToMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_segmentedControl selectedSegmentIndex] == 0)
        return self.memoryList.count;
    return self.memoryLabelsList.count/2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *memoryPosition = (UILabel *) [cell viewWithTag:1];
    memoryPosition.textColor = [UIColor blackColor];
    
    if ([_segmentedControl selectedSegmentIndex] == 0)
    {
        memoryPosition.text = [NSString stringWithFormat:@"%@  -  %@", [self binaryStringFromInteger:(int)indexPath.row], self.memoryList [indexPath.row]];
        if (self.currentOperation == indexPath.row)
            memoryPosition.textColor = [UIColor colorWithRed:189.0/255.0 green:32.0/255.0 blue:38.0/255.0 alpha:1];
    }
    else
    {
        memoryPosition.text = [NSString stringWithFormat:@"%@  -  %@", [self binaryStringFromInteger:(int)indexPath.row], self.memoryLabelsList[indexPath.row*2]];
    }
    
    return cell;
}

-(NSString * )binaryStringFromInteger:(int) aNumber
{
    NSMutableString * string = [[NSMutableString alloc] init];
    
    int spacing = pow( 2, 3 );
    int width = ( sizeof( aNumber ) ) * spacing;
    int binaryDigit = 0;
    int integer = aNumber;
    
    while( binaryDigit < width )
    {
        binaryDigit++;
        
        [string insertString:( (integer & 1) ? @"1" : @"0" )atIndex:0];
        
        if( binaryDigit % spacing == 0 && binaryDigit != width )
        {
            [string insertString:@" " atIndex:0];
        }
        
        integer = integer >> 1;
    }
    
    NSArray *myWords = [string componentsSeparatedByString:@" "];
    
    
    return myWords[myWords.count -1];
}

- (void)viewSettings
{
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

- (IBAction)switchMemory:(UISegmentedControl *)sender {
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self viewSettings];
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self viewSettings];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

@end
