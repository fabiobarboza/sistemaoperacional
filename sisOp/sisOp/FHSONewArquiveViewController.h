//
//  FHSONewArquiveViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/18/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSONewArquiveViewController : UIViewController <UITextFieldDelegate>

@end
