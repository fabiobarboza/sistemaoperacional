//
//  FHSOUserArquivesViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/13/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSOUserArquivesViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@end
