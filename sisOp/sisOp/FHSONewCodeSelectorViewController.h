//
//  FHSONewCodeSelectorViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSOCode.h"

@interface FHSONewCodeSelectorViewController : UIViewController

@property (nonatomic) FHSOCode *code;

@end
