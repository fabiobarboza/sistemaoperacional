//
//  FHSOMemory.m
//  sisOp
//
//  Created by Fabio Barboza on 3/6/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOMemory.h"
#import "FHSOIndexToMemory.h"

@implementation FHSOMemory
{

}

@synthesize pageLength = _pageLength;

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.memory = [[NSMutableArray alloc] init];
        self.freePosition = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)setNumberOfPages:(int)numberOfPages
{
    _numberOfPages = numberOfPages;
    self.pagesFree = numberOfPages;
    for (int i = 0; i < numberOfPages; i++)
    {
        [self.freePosition addObject:[[NSNumber alloc] initWithInt:0]];
    }
}

- (int)getIndexFromFreePage
{
    for (int i = 0; i < self.freePosition.count; i++)
    {
        if ([self.freePosition[i] intValue] == 0)
        {
            self.freePosition[i] = [[NSNumber alloc] initWithInt:1];
            self.pagesFree--;
            return i;
        }
    }
    return -1;
}

- (void)deallocMemoryPositionFromArrayWithIndex:(NSArray *)listToDealloc
{
    FHSOIndexToMemory *indexToMemory;
    for (int i = 0; i < listToDealloc.count; i++)
    {
        indexToMemory = listToDealloc[i];
        self.memory[indexToMemory.pageNumber] = [[NSNumber alloc] initWithInt:0];
        self.freePosition[indexToMemory.pageNumber] = [[NSNumber alloc] initWithInt:0];
        self.pagesFree++; 
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.memory forKey:@"memory"];
    [aCoder encodeObject:self.freePosition forKey:@"freePosition"];
    [aCoder encodeInt:self.numberOfPages forKey:@"numberOfPages"];
    [aCoder encodeInt:self.pageLength forKey:@"pageLength"];
    [aCoder encodeInt:self.pagesFree forKey:@"pagesFree"];
    [aCoder encodeBool:self.startedExecution forKey:@"startedExecution"];
    [aCoder encodeInt:self.lastPage forKey:@"lastPage"];
    [aCoder encodeInt:self.totalPageCount forKey:@"totalPage"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        self.memory = [decoder decodeObjectForKey:@"memory"];
        self.freePosition = [decoder decodeObjectForKey:@"freePosition"]; 
        self.numberOfPages = [decoder decodeIntForKey:@"numberOfPages"];
        self.pageLength = [decoder decodeIntForKey:@"pageLength"];
        self.lastPage = [decoder decodeIntForKey:@"lastPage"];
        self.pagesFree = [decoder decodeIntForKey:@"pagesFree"];
        self.startedExecution = [decoder decodeBoolForKey:@"startedExecution"];
        self.totalPageCount = [decoder decodeIntForKey:@"totalPage"]; 
    }
    return self;
}

- (void)storeInDefaults
{
     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
     [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"memory"];
}

- (void)removeFromDefaults
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"memory"]; 
}


@end
