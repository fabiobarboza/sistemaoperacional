//
//  FHSOMemory.h
//  sisOp
//
//  Created by Fabio Barboza on 3/6/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHSOMemory : NSObject<NSCoding>
@property (nonatomic) NSMutableArray *memory;
@property (nonatomic) NSMutableArray *freePosition;
@property (nonatomic) int numberOfPages;
@property (nonatomic) int pageLength;
@property (nonatomic) int pagesFree;
@property (nonatomic) int lastPage;
@property (nonatomic) int totalPageCount; 
@property (nonatomic) BOOL startedExecution; 
- (int)getIndexFromFreePage;
- (void)deallocMemoryPositionFromArrayWithIndex:(NSArray *)listToDealloc;
- (void)storeInDefaults;
- (void)removeFromDefaults;
@end
