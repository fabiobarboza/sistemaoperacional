//
//  FHSOInfoViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 4/3/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSOInfoViewController : UIViewController <UIScrollViewDelegate>

@end
