//
//  FHSONewDataViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/13/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSONewDataViewController.h"
#import "FHSOCode.h"

@interface FHSONewDataViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtVariableName;
@property (weak, nonatomic) IBOutlet UILabel *lblVariableValue;
@property (weak, nonatomic) IBOutlet UIStepper *valueStepper;

@end

@implementation FHSONewDataViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self verifyEditing];
}

- (void) verifyEditing
{
    if (_isEditing)
    {
        _txtVariableName.text = [_code.dataList objectAtIndex:_dataIndexAtDataList*2];
        _lblVariableValue.text = [_code.dataList objectAtIndex:(_dataIndexAtDataList*2) +1];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)stepperValueChanged:(UIStepper *)sender
{
    self.lblVariableValue.text = [NSString stringWithFormat:@"%d",(int)sender.value];
}

- (IBAction)btnConfirm:(id)sender {
    if (_isEditing)
    {
        _code.dataList[_dataIndexAtDataList*2] = _txtVariableName.text;
        _code.dataList[(_dataIndexAtDataList*2) + 1] = _lblVariableValue.text;
    }
    else
    {
        [_code.dataList addObject:_txtVariableName.text];
        [_code.dataList addObject:_lblVariableValue.text];
    }
    [_code storeInDefaults];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
