//
//  FHSOEntranceViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOEntranceViewController.h"

@interface FHSOEntranceViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *tapButton;

@end

@implementation FHSOEntranceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(tapEffect)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)tapEffect
{
    _tapButton.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        _tapButton.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    [UIView animateWithDuration:1.0 animations:^{
        _tapButton.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
