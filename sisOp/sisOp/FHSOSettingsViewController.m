//
//  FHSOSettingsViewController.m
//  sisOp
//
//  Created by Bruno Vieira Bulso on 03/04/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOSettingsViewController.h"

@interface FHSOSettingsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmented;

@end

@implementation FHSOSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)clickDone:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)valueChanged:(id)sender {
    if(self.segmented.selectedSegmentIndex == 0)
    {
        self.lblValue.hidden = NO;
        self.stepper.hidden = NO;
        self.lblType.text = @"Quantum Value";
    }
    else
    {
        self.stepper.hidden = YES;
        self.lblValue.hidden = YES;
        self.lblType.text = @"SJS Mode ON";
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"sjs"];
    }
}

- (IBAction)stepperValueChanged:(id)sender {
    self.lblValue.text = [NSString stringWithFormat:@"%d",(int)self.stepper.value];
    [[NSUserDefaults standardUserDefaults] setObject:self.lblValue.text forKey:@"quantum"];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"sjs"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    // Do any additional setup after loading the view.
}


@end
