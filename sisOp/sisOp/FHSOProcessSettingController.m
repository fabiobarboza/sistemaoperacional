//
//  FHSOProcessSettingController.m
//  sisOp
//
//  Created by Bruno Vieira Bulso on 03/04/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOProcessSettingController.h"

@interface FHSOProcessSettingController ()
@property (weak, nonatomic) IBOutlet UILabel *lblArrivalTimerCounter;
@property (weak, nonatomic) IBOutlet UILabel *lblArrivalTime;
@property (weak, nonatomic) IBOutlet UIStepper *stpArrivalTimer;

@end

@implementation FHSOProcessSettingController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    self.navigationController.navigationBarHidden = NO;
}

- (void)configureView
{
    if (_codeProcess.willExecute)
    {
        _stpArrivalTimer.enabled = NO;
    }
    _lblArrivalTimerCounter.text = [NSString stringWithFormat:@"%d", _codeProcess.arrivalTime];

}
- (IBAction)strArrivalChanged:(UIStepper *)sender
{
            _codeProcess.arrivalTime = (int)sender.value;
            _lblArrivalTimerCounter.text = [NSString stringWithFormat:@"%d", _codeProcess.arrivalTime];
       
}
- (IBAction)strProcessChanged:(UIStepper *)sender
{
    _codeProcess.processTime = (int)sender.value;
}


- (IBAction)btnDone:(UIBarButtonItem *)sender
{
    if (!_codeProcess.willExecute)
    {
        _codeProcess.willExecute = YES; 
        [_codeProcess storeInDefaults];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


@end
