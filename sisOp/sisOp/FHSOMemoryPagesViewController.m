//
//  FHSOMemoryPagesViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 5/10/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOMemoryPagesViewController.h"
#import "FHSOMemoryViewController.h"
#import "FHSOMemory.h"
#import "FHSOIndexToMemory.h"

@interface FHSOMemoryPagesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FHSOMemoryPagesViewController
{
    NSMutableArray *tableViewList;
    FHSOMemory *memory;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureTableView];
}

- (void)configureTableView
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self configureMemory];
    [self loadTableViewList];
}

- (void)configureMemory
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"memory"];
    if (data != nil)
        memory = [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)loadTableViewList
{
    tableViewList = [[NSMutableArray alloc] init];
    if (memory != nil)
    {
        tableViewList = memory.memory;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if ([tableViewList[indexPath.row] isKindOfClass:[NSArray class]])
    {
        FHSOIndexToMemory *indexToMemory = tableViewList[indexPath.row][0];
        
        UILabel *lblProcessName = (UILabel *)[cell viewWithTag:1];
        lblProcessName.text = indexToMemory.processName;
        
        UILabel *lblMemoryNumber = (UILabel *)[cell viewWithTag:2];
        lblMemoryNumber.text = [self binaryStringFromInteger:(int)indexPath.row];
        
        UILabel *lblProcessPC = (UILabel *)[cell viewWithTag:3];
        lblProcessPC.text = [NSString stringWithFormat:@"PC: %d", indexToMemory.programCounterInPage];
        
        UILabel *lblProcessPage = (UILabel *)[cell viewWithTag:4];
        lblProcessPage.text = [NSString stringWithFormat:@"Page: %d",indexToMemory.pageNumber];
    }
    else
    {
        UILabel *lblProcessName = (UILabel *)[cell viewWithTag:1];
        lblProcessName.text = @"Empty";
        
        UILabel *lblMemoryNumber = (UILabel *)[cell viewWithTag:2];
        lblMemoryNumber.text = [self binaryStringFromInteger:(int)indexPath.row];
        
        UILabel *lblProcessPC = (UILabel *)[cell viewWithTag:3];
        lblProcessPC.text = @"PC: 0";
        UILabel *lblProcessPage = (UILabel *)[cell viewWithTag:4];
        lblProcessPage.text = @"Page: 0";
    }

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableViewList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (IBAction)backToMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString * )binaryStringFromInteger:(int) aNumber
{
    NSMutableString * string = [[NSMutableString alloc] init];
    
    int spacing = pow( 2, 3 );
    int width = ( sizeof( aNumber ) ) * spacing;
    int binaryDigit = 0;
    int integer = aNumber;
    
    while( binaryDigit < width )
    {
        binaryDigit++;
        
        [string insertString:( (integer & 1) ? @"1" : @"0" )atIndex:0];
        
        if( binaryDigit % spacing == 0 && binaryDigit != width )
        {
            [string insertString:@" " atIndex:0];
        }
        
        integer = integer >> 1;
    }
    
    NSArray *myWords = [string componentsSeparatedByString:@" "];
    
    
    return myWords[myWords.count -1];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableViewList[indexPath.row] isKindOfClass:[NSArray class]])
    {
         [self performSegueWithIdentifier:@"processMemorySegue" sender:indexPath];
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSIndexPath*)sender
{
    FHSOMemoryViewController *controller = (FHSOMemoryViewController*)segue.destinationViewController;
        FHSOIndexToMemory *index = tableViewList[sender.row][0];
        controller.memoryList = tableViewList[sender.row][1];
        controller.currentOperation = index.programCounterInPage;
    controller.memoryLabelsList = index.labelList;
}




@end
