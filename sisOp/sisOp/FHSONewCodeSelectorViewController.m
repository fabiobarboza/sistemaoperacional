//
//  FHSONewCodeSelectorViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSONewCodeSelectorViewController.h"
#import "FHSONewDataViewController.h"
#import "FHSONewCodeViewController.h"
#import "FHSONewLabelViewController.h"

@interface FHSONewCodeSelectorViewController ()

@end

@implementation FHSONewCodeSelectorViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)btnNewInstruction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"instruction" sender:nil];
}

- (IBAction)btnNewData:(UIButton *)sender {
    [self performSegueWithIdentifier:@"data" sender:nil];
}

- (IBAction)btnNewLabel:(UIButton *)sender {
    [self performSegueWithIdentifier:@"createLabelSegue" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"data"])
    {
        FHSONewDataViewController *data = (FHSONewDataViewController*)segue.destinationViewController;
        data.code = _code;
    }
    else if ([segue.identifier isEqualToString:@"instruction"])
    {
        FHSONewCodeViewController *instruction = (FHSONewCodeViewController*)segue.destinationViewController;
        instruction.code = _code;
    }
    else if ([segue.identifier isEqualToString:@"createLabelSegue"])
    {
        FHSONewLabelViewController *labelVC = (FHSONewLabelViewController *)segue.destinationViewController;
        labelVC.code = _code;
    }
}

@end
