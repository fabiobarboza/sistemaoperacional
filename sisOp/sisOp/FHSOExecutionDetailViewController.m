//
//  FHSOExecutionDetailViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 5/10/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOExecutionDetailViewController.h"
#import "FHSOCodeStatistics.h"

@interface FHSOExecutionDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FHSOExecutionDetailViewController
{
    NSMutableArray *tableViewList;
}

- (void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureTableView];
}

- (void)configureTableView
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self loadTableViewData];
}

- (void)loadTableViewData
{
    tableViewList = [[NSMutableArray alloc] init];
    
    tableViewList = self.codeStatisctsList;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    FHSOCodeStatistics *myCode = tableViewList[indexPath.row];
    
    UILabel *lblProcessName = (UILabel *)[cell viewWithTag:1];
    lblProcessName.text = myCode.name;
    
    UILabel *lblProcessTime = (UILabel *)[cell viewWithTag:2];
    lblProcessTime.text = [NSString stringWithFormat:@"TT: %d", (myCode.executeTime + myCode.waitingTime)];
    
    UILabel *lblProcessWT = (UILabel *)[cell viewWithTag:18];
    lblProcessWT.text = [NSString stringWithFormat:@"WT: %d", myCode.waitingTime];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 69;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableViewList.count;
}

@end
