//
//  FHSORoundRobin.h
//  sisOp
//
//  Created by Bruno Vieira Bulso on 03/04/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FHSOCode.h"
#import "FHSOMemory.h"
#import "FHSOIndexToMemory.h"

@interface FHSORoundRobin : NSObject
@property (nonatomic) NSMutableArray *codeList; 
@property (nonatomic) NSMutableArray *codeExecuteList;
@property (nonatomic) NSMutableArray *suspendedList;
@property (nonatomic) NSMutableArray *codeStatisctsList; 
@property (nonatomic) int totalTime;
@property (nonatomic) int quantum; 
@property (nonatomic) int processExecutionTime;
@property (nonatomic) BOOL finished; 
@property (nonatomic) FHSOMemory *memory; 
- (FHSOCode*)checkRoundRobin;
- (void)startRoundRobin;
- (void)saveExecuteListInDefaults;
@end
