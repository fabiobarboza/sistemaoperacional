//
//  FHSOAssemblerGuideViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOAssemblerGuideViewController.h"
#import "FHSOAssemblerGuide.h"
#import "FHSOInstruction.h"
#import "FHSOInstructionDetailViewController.h"

@interface FHSOAssemblerGuideViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSMutableArray *instructionList;
@property (nonatomic) FHSOAssemblerGuide *guide;

@end

@implementation FHSOAssemblerGuideViewController

- (void)viewSettings
{
    [self loadInformationList];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void)loadInformationList
{
    _instructionList = [[NSMutableArray alloc] init];
    _guide = [[FHSOAssemblerGuide alloc] init];
    _instructionList = [_guide createInformations];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO; 
	// Do any additional setup after loading the view.
    
    [self viewSettings];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _instructionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    FHSOInstruction *currentInstruction = _instructionList[indexPath.row];
    
    UILabel *memoryPosition = (UILabel *) [cell viewWithTag:1];
    memoryPosition.text = currentInstruction.name;
    
    
    
    return cell;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"details" sender:indexPath];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSIndexPath*)sender
{
    FHSOInstructionDetailViewController *nextViewController = segue.destinationViewController;
    
    FHSOInstruction *currentInstruction = _instructionList[sender.row];
    
    nextViewController.currentInstruction = currentInstruction;
    
    

}

@end
