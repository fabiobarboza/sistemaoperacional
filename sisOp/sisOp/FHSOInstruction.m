//
//  FHSOInstruction.m
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOInstruction.h"

@implementation FHSOInstruction

- (instancetype)initWithName:(NSString *)aName andDescription:(NSString *)aDescription andCategory:(NSString *)aCategory andFunction:(NSString *)aFunction
{
    self = [super init];
    if (self)
    {
        self.name = aName;
        self.description = aDescription;
        self.category = aCategory;
        self.function = aFunction;
    }
    return self;
}

@end
