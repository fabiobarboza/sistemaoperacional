//
//  FHSOUserArquivesViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/13/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOUserArquivesViewController.h"
#import "FHSOCode.h"
#import "FHSOReadFileViewController.h"

@interface FHSOUserArquivesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FHSOUserArquivesViewController
{
    NSMutableArray *codeList;
    NSMutableArray *listFromDefaults;
    FHSOCode *currentCode;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void) initMemoryList
{
    listFromDefaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"codeList"];
    for (NSData *data in listFromDefaults)
    {
       [codeList addObject:[FHSOCode transformDataIntoClass:data]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}

- (void)configureView
{
    
    self.navigationController.navigationBarHidden = YES;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    listFromDefaults = [[NSMutableArray alloc] init];
    codeList = [[NSMutableArray alloc] init];
    [self initMemoryList];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (IBAction)backToMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        FHSOCode *code = [codeList objectAtIndex:indexPath.row];
        [code removeFromDefaults];
        [codeList removeObject:code];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
        return @"Archives";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return codeList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UILabel *memoryPosition = (UILabel *) [cell viewWithTag:1];
    memoryPosition.textColor = [UIColor blackColor];

    memoryPosition.text = ((FHSOCode*)codeList[indexPath.row]).name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"archiveDetailSegue" sender:indexPath];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSIndexPath*)sender
{
    if ([segue.identifier isEqualToString:@"archiveDetailSegue"])
    {
        FHSOReadFileViewController *readFile = (FHSOReadFileViewController*)segue.destinationViewController;
            readFile.code = codeList[sender.row];
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    [self configureView];
    [_tableView reloadData];
}

@end
