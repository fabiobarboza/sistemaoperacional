//
//  FHSOProcessSettingController.h
//  sisOp
//
//  Created by Bruno Vieira Bulso on 03/04/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSOCode.h"

@interface FHSOProcessSettingController : UIViewController
@property FHSOCode *codeProcess;
@end
