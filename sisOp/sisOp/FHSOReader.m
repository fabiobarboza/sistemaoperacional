//
//FHSOReader.m
//sisOp
//
//Created by Fabio Barboza on 3/6/14.
//Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOReader.h"

@implementation FHSOReader
{
    NSArray *linesFromArchive;
    NSMutableArray *memoryWithInstructionsAndVariables;
    NSMutableArray *memoryWithLabel;
    NSMutableArray *memoryWithData;
}

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

//This method read the file and start the process to store all data into arrays
-(void)readFile
{
    [[NSUserDefaults standardUserDefaults]setObject:[[NSNumber alloc]initWithInt:0] forKey:@"currentOperation"];

    NSError *error;

    NSString* textFromUserDefaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"assemblyFromText"];
    if (textFromUserDefaults != nil)
        [self readInstructionsVariablesAndLabel : textFromUserDefaults];
    else
    {
        
        NSString *filename = @"assembly.txt";
        NSString *path = [[NSBundle mainBundle] pathForResource:[filename stringByDeletingPathExtension] ofType:[filename pathExtension]];
        NSString *fileText = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        [[NSUserDefaults standardUserDefaults] setObject:fileText forKey:@"assemblyFromText"];
        [self readInstructionsVariablesAndLabel : fileText];
    }    
}

- (void) readInstructionsVariablesAndLabel : (NSString*) textFromFile
{
    //convert the context of file into array separated by lines
    linesFromArchive =
    [textFromFile componentsSeparatedByCharactersInSet:
     [NSCharacterSet newlineCharacterSet]];
    //init arrays
    memoryWithInstructionsAndVariables = [[NSMutableArray alloc] init];
    memoryWithLabel = [[NSMutableArray alloc] init];
    //goes through all rows
    for (int i = 0; i < linesFromArchive.count; i++)
    {
        //convert the context from each row into array
        NSMutableArray* wordsFromLine = [[NSMutableArray alloc] initWithArray:
                                         [linesFromArchive[i] componentsSeparatedByCharactersInSet:
                                          [NSCharacterSet whitespaceCharacterSet]]];
        //if passed through all instructions will start to store data
        if ([wordsFromLine[0] isEqualToString:@".data"])
        {
            [self readData: i+1];
            break;
        }
        //if the first word from line isn't empty or .code/.endcode will store the instruction
        else if ([wordsFromLine[0] length] > 0 && ![wordsFromLine[0] isEqualToString:@".code"] && ![wordsFromLine[0] isEqualToString:@".endcode"])
        {
            //take the last character from the first word
            NSString *lastCharFromWord = [wordsFromLine[0] substringFromIndex:[wordsFromLine[0] length] - 1];
            //if the last character contains : is a Label and store in another structure
            if ([lastCharFromWord isEqualToString:@":"])
            {
                NSString *substring = [wordsFromLine[0] substringWithRange:NSMakeRange(0, [wordsFromLine[0] length] -1)];
                memoryWithLabel[memoryWithLabel.count] = [[NSMutableArray alloc] initWithObjects:substring,[[NSNumber alloc] initWithInt: memoryWithInstructionsAndVariables.count],nil];
//                [memoryWithLabel setObject:[[NSNumber alloc] initWithInt: memoryWithInstructionsAndVariables.count] forKey:wordsFromLine[0]];
                [wordsFromLine removeObject: wordsFromLine[0]];
            }
            //save the instructions and variables
            [memoryWithInstructionsAndVariables addObject: wordsFromLine];
        }
    }
    
}

- (void) readData : (int) numberOfLineInFile
{
    //init array
    memoryWithData = [[NSMutableArray alloc] init];
    //goes through all rows
    for (int i = numberOfLineInFile; i < linesFromArchive.count; i++)
    {
        //convert the context from each row into array
        NSMutableArray* wordsFromLine = [[NSMutableArray alloc] initWithArray:
                                         [linesFromArchive[i] componentsSeparatedByCharactersInSet:
                                          [NSCharacterSet whitespaceCharacterSet]]];
         //if the first word from line is .enddata, we already read all lines
        if ([wordsFromLine[0] isEqualToString:@".enddata"])
        {
            break;
        }
        //save data
        else if ([wordsFromLine[0] length] > 0)
        {
            [memoryWithData addObject: wordsFromLine];
        }
    }
    //call this method to persistent the lists
    [self storeAllDatas];
}

- (void) storeAllDatas
{
    //persist the lists
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"memoryWithInstructions"];
    [[NSUserDefaults standardUserDefaults] setObject:memoryWithInstructionsAndVariables forKey:@"memoryWithInstructions"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"memoryLabel"];
    [[NSUserDefaults standardUserDefaults] setObject:memoryWithLabel forKey:@"memoryLabel"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"memoryData"];
    [[NSUserDefaults standardUserDefaults] setObject:memoryWithData forKey:@"memoryData"];
    
}


@end