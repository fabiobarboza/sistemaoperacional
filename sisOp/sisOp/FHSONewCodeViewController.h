//
//  FHSONewCodeViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/13/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSOCode.h"

@interface FHSONewCodeViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

@property (nonatomic) FHSOCode *code;
@property (nonatomic) int operationIndexInInstructionList;
@property (nonatomic) BOOL isEditing;

@end
