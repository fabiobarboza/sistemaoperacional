//
//  FHSOMainPageViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/7/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSOMemory.h"

@interface FHSOMainPageViewController : UIViewController
@property (nonatomic) FHSOMemory *memory;
@end
