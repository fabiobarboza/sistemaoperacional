//
//  FHSOAssemblerGuide.m
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOAssemblerGuide.h"
#import "FHSOInstruction.h"

@implementation FHSOAssemblerGuide

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

-(NSMutableArray *)createInformations
{
    NSMutableArray *list = [[NSMutableArray alloc] init];
    FHSOInstruction *instruction;
    
    instruction = [[FHSOInstruction alloc] initWithName:@"ADD op1" andDescription:@"The accumulator has its value added for operating" andCategory:@"Arithmetics" andFunction:@"ACC = ACC + (OP1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"SUB op1" andDescription:@"The accumulator deducts the content from operating" andCategory:@"Arithmetics" andFunction:@"ACC = ACC - (OP1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"MULT op1" andDescription:@"The accumulator has its value multiplied for operating" andCategory:@"Arithmetics" andFunction:@"ACC = ACC * (OP1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"DIV op1" andDescription:@"The accumulator has its value divided for operating" andCategory:@"Arithmetics" andFunction:@"ACC = ACC / (OP1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"LOAD op1" andDescription:@"The accumulator receives the content from operation" andCategory:@"Memory" andFunction:@"ACC = (OP1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"STORE op1" andDescription:@"The operating receives the content from accumulator" andCategory:@"Memory" andFunction:@"(OP1) = ACC"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"BRANY label" andDescription:@"The PC receives label`s position" andCategory:@"Jump" andFunction:@"PC <- label"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"BRPOS label" andDescription:@"If the content of the accumulator is greater that zero the PC receives label`s position" andCategory:@"Jump" andFunction:@"If( ACC > 0 ) Then PC <- (op1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"BRZERO label" andDescription:@"If the content of the accumulator is equals to zero the PC receives label`s position" andCategory:@"Jump" andFunction:@"If( ACC == 0 ) Then PC <- (op1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"BRNEG label" andDescription:@"If the content of the accumulator is minor that zero the PC receives label`s position" andCategory:@"Jump" andFunction:@"If( ACC < 0 ) Then PC <- (op1)"];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@"SYSCALL index" andDescription:@"System call" andCategory:@"System" andFunction:@""];
    [list addObject:instruction];
    
    instruction = [[FHSOInstruction alloc] initWithName:@".data" andDescription:@"Data area" andCategory:@"System" andFunction:@""];
    [list addObject:instruction];
    
    return list;
}

@end
