//
//  FHSOAssemblerGuide.h
//  sisOp
//
//  Created by Fabio Barboza on 3/11/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHSOAssemblerGuide : NSObject
@property (nonatomic) NSMutableArray *operationList;

-(NSMutableArray *)createInformations;

@end
