//
//  FHSOReadFileViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/7/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHSOCode.h"

@interface FHSOReadFileViewController : UIViewController <UITextViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) FHSOCode *code;
@end
