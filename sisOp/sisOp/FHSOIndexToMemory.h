//
//  FHSOIndexToMemory.h
//  sisOp
//
//  Created by Bruno Vieira Bulso on 4/28/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHSOIndexToMemory : NSObject<NSCoding>
@property (nonatomic) NSString* processName;
@property (nonatomic) NSMutableArray *labelList; 
@property (nonatomic) int pageNumber;
@property (nonatomic) int programCounterInPage;
@end
