//
//  FHSONewCodeViewController.m
//  sisOp
//
//  Created by Fabio Barboza on 3/13/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSONewCodeViewController.h"

@interface FHSONewCodeViewController ()
{
    NSMutableArray *operations;
    int currentOperation;
    int currentVariable;
    int currentLabel;
    int countComponentsLoadInPickerView;
}

@property (weak, nonatomic) IBOutlet UIPickerView *operationPickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *variablePickerView;
@property (weak, nonatomic) IBOutlet UIStepper *constantStepper;
@property (weak, nonatomic) IBOutlet UILabel *lblOperatorTitle;
@property (weak, nonatomic) IBOutlet UISegmentedControl *operatorTypeSegmented;
@property (weak, nonatomic) IBOutlet UILabel *lblConstant;

@end

@implementation FHSONewCodeViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self loadDataFromUserDefaults];
    
    self.navigationController.navigationBarHidden = NO;
    
    self.operationPickerView.delegate = self;
    self.operationPickerView.dataSource = self;
    self.variablePickerView.delegate = self;
    self.variablePickerView.dataSource = self;
    
    self.operatorTypeSegmented.selectedSegmentIndex = 0;
    [self SegmentedValueChanged:self.operatorTypeSegmented];
    [self SegmentedValueChanged:self.operatorTypeSegmented];
}

- (void)verifyEditing
{
    if (_isEditing)
    {
        NSString *operationName = [_code.instructionList objectAtIndex:_operationIndexInInstructionList*2];
        NSString *data = [_code.instructionList objectAtIndex:(_operationIndexInInstructionList *2 )+1];
        if ([data characterAtIndex:0] == '#')
        {
            _lblConstant.text = data;
            self.lblOperatorTitle.text = @"Constants";
            self.operatorTypeSegmented.selectedSegmentIndex = 1;
            self.lblConstant.hidden = NO;
            self.constantStepper.hidden = NO;
            self.variablePickerView.hidden = YES;
        }
        else
        {
            _operatorTypeSegmented.selectedSegmentIndex = 0;
            for (int i = 0; i > self.code.dataList.count;i++)
            {
                if ([self.code.dataList[i] isEqualToString:data])
                {
                    [_operationPickerView selectRow:i inComponent:0 animated:YES];
                    currentVariable = i;
                }
            }
        }
        for (int i = 0; i < operations.count;i++)
        {
            if ([operations[i] isEqualToString:operationName])
            {
                [_operationPickerView selectRow:i inComponent:0 animated:YES];
                currentOperation = i;
            }
        }
    }

}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (IBAction)stepperValueChanged:(UIStepper *)sender {
    self.lblConstant.text = [NSString stringWithFormat:@"#%d",(int)sender.value];
}

-(void)viewWillAppear:(BOOL)animated
{
        [self verifyEditing];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 1)
    {
        if (row > 5 && row < 9)
        {
            self.operatorTypeSegmented.enabled = NO;
            self.lblOperatorTitle.text = @"Labels";
            self.lblConstant.hidden = YES;
            self.constantStepper.hidden = YES;
            self.variablePickerView.hidden = NO;
            [self.variablePickerView reloadAllComponents];
        }
        else if(row == 9)
        {
            self.lblOperatorTitle.text = @"Constants";
            self.operatorTypeSegmented.selectedSegmentIndex = 1;
            self.operatorTypeSegmented.enabled = NO;
            self.lblConstant.hidden = NO;
            self.constantStepper.hidden = NO;
            self.variablePickerView.hidden = YES;
        }
        else
        {
            self.lblOperatorTitle.text = @"Variables";
            [self.variablePickerView reloadAllComponents];
            self.operatorTypeSegmented.enabled = YES;
        }
        currentOperation = (int)row;
    }
    else
    {
        if (self.operatorTypeSegmented.selectedSegmentIndex == 0)
            currentVariable = (int)row;
        else if (!self.operatorTypeSegmented.enabled)
            currentLabel = (int)row;
    }
}
- (IBAction)confirm:(UIButton *)sender {
    
    NSString *newOperation;
    NSString *newValue;
    if (self.operatorTypeSegmented.selectedSegmentIndex == 0)
    {
        newOperation = operations[currentOperation];
        if (self.code.dataList.count > 0)
        newValue = self.code.dataList[currentVariable *2];
        else
        {
            UIAlertView* alert;
            alert = [[UIAlertView alloc] initWithTitle:@"Ops!" message:@"Você não possui nenhuma variável!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return; 
        }
    }
    else if (self.operatorTypeSegmented.selectedSegmentIndex == 1)
    {
        newOperation = operations[currentOperation];
        newValue = self.lblConstant.text;
    }
    else
    {
        newOperation = operations[currentOperation];
        newValue = self.code.labelList[currentLabel*2];
    }
    if (_isEditing)
    {
        self.code.instructionList[_operationIndexInInstructionList *2] = newOperation;
        self.code.instructionList[(_operationIndexInInstructionList*2) + 1] = newValue;
    }
    else
    {
        [self.code.instructionList addObject: newOperation];
        [self.code.instructionList addObject: newValue];
    }
    [self.code storeInDefaults];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmAction:(UIBarButtonItem *)sender
{
    
}


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag == 1)
    {
        return operations[row];
    }
    else if (self.operatorTypeSegmented.enabled == YES)
    {
        return self.code.dataList[row * 2];
    }
    return self.code.labelList[row *2];
}

- (IBAction)SegmentedValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0)
    {
        self.lblOperatorTitle.text = @"Variable";
        self.lblConstant.hidden = YES;
        self.constantStepper.hidden = YES;
        self.variablePickerView.hidden = NO;
        [self.variablePickerView reloadAllComponents];
    }
    else
    {
        self.lblOperatorTitle.text = @"Constants";
        self.lblConstant.hidden = NO;
        self.constantStepper.hidden = NO;
        self.variablePickerView.hidden = YES;
    }
}

- (void)loadDataFromUserDefaults
{
    operations = [[NSMutableArray alloc] initWithObjects:@"ADD",
                  @"SUB",
                  @"MULT",
                  @"DIV",
                  @"LOAD",
                  @"STORE",
                  @"BRANY",
                  @"BRPOS",
                  @"BRNEG",
                  @"SYSCALL",
                  nil];
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        return operations.count;
    }
    else if(self.operatorTypeSegmented.enabled == YES)
    {
        return self.code.dataList.count /2;
    }
    else if(self.operatorTypeSegmented.enabled == NO)
    {
        return self.code.labelList.count /2;
    }
    return 0;
}




@end
