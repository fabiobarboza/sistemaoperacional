//
//  FHSOIndexToMemory.m
//  sisOp
//
//  Created by Bruno Vieira Bulso on 4/28/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSOIndexToMemory.h"

@implementation FHSOIndexToMemory
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_processName forKey:@"processName"];
    [encoder encodeInt:_pageNumber forKey:@"pageNumber"];
    [encoder encodeInt:_programCounterInPage forKey:@"programCounter"];
    [encoder encodeObject:_labelList forKey:@"label"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        _processName = [decoder decodeObjectForKey:@"processName"];
        _pageNumber = [decoder decodeIntForKey:@"pageNumber"];
        _programCounterInPage = [decoder decodeIntForKey:@"programCounter"];
        _labelList = [decoder decodeObjectForKey:@"label"]; 
    }
    return self;
}

@end
