//
//  FHSOMemoryPagesViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 5/10/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSOMemoryPagesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
