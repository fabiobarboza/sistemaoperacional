//
//  FHSORoundRobin.m
//  sisOp
//
//  Created by Bruno Vieira Bulso on 03/04/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHSORoundRobin.h"
#import "FHSOCodeStatistics.h"

@implementation FHSORoundRobin
{
    BOOL largeCode;
}


- (instancetype) init
{
    if (self)
    {
        self.codeList = [[NSMutableArray alloc] init];
        self.codeExecuteList = [[NSMutableArray alloc] init];
        self.suspendedList = [[NSMutableArray alloc] init]; 
        self.memory = [[FHSOMemory alloc] init];
        self.codeStatisctsList = [[NSMutableArray alloc] init];
        self.quantum = [[[NSUserDefaults standardUserDefaults] objectForKey:@"quantum"] intValue];
        self.processExecutionTime = 0;
        self.totalTime = 0;
        self.finished = NO;
        largeCode = NO;
    }
    return self;
}


- (void)startRoundRobin
{
    [self insertValuesInExecuteList];
    [self verifyExecuteList];
}

- (void)insertValuesInExecuteList
{
    NSMutableArray *listFromDefaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"codeList"];
    for (NSData *data in listFromDefaults)
    {
        FHSOCode *myCode = [FHSOCode transformDataIntoClass:data];
        if (myCode.willExecute)
        {
            [self.codeList addObject: myCode];
        }
    }
}

- (void)verifyExecuteList
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"executeList"];
    if (data != nil)
    {
        self.codeExecuteList = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    NSData *data2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"suspendedList"];
    if (data2 != nil)
    {
        self.suspendedList = [NSKeyedUnarchiver unarchiveObjectWithData:data2]; 
    }
    NSData *data3 = [[NSUserDefaults standardUserDefaults] objectForKey:@"statisctsList"];
    if (data3 != nil)
    {
        self.codeStatisctsList = [NSKeyedUnarchiver unarchiveObjectWithData:data3];
    }
}

#pragma mark - Execute Round Robin

- (FHSOCode*)checkRoundRobin
{
    [self managerExecuteList];
    [self adjustExecuteList];
    [self verifyPrioritesFromExecuteListComparesWithSuspendedList];
    return [self getCodeToExecute];
}

#pragma mark - Manager List

- (void)adjustCodeList
{
    FHSOCode *firstCode;
    FHSOCode *secondCode;
    for (int i = 0; i < self.codeList.count; i++)
    {
        for (int j = 0; j < self.codeList.count-1; j++)
        {
            firstCode = self.codeList[j];
            secondCode = self.codeList[j+1];
            if (firstCode.arrivalTime > secondCode.arrivalTime)
            {
                FHSOCode *aux = firstCode;
                self.codeList[j] = secondCode;
                self.codeList[j+1] = aux;
            }
        }
    }
}

- (void)managerExecuteList
{
    int time = self.totalTime;
    
    [self adjustCodeList];
    if (self.codeList.count != 0)
    {
        do
        {
            for (int i = 0; i < self.codeList.count; i++)
            {
                FHSOCode *myCode = self.codeList[i];
                if (((FHSOCode *)[self.codeList lastObject]).arrivalTime < time)
                {
                    time = 0;
                    return;
                }
                if (myCode.arrivalTime == time)
                {
                    if (![self.codeExecuteList containsObject:myCode])
                    {
                        myCode.lastTime = self.totalTime;
                        [myCode storeInDefaults];
                        myCode.pageLength = self.memory.pageLength;
                        [myCode separeCodeIntoPages];
                        if (myCode.pagesList.count <= self.memory.pagesFree)
                        {
                            for (int i = 0; i < myCode.pagesList.count; i++)
                            {
                                 ((FHSOIndexToMemory*) myCode.pagesList[i][0]).labelList = myCode.labelList;
                                ((FHSOIndexToMemory*) myCode.pagesList[i][0]).pageNumber = [self.memory getIndexFromFreePage];
                                self.memory.memory[((FHSOIndexToMemory*) myCode.pagesList[i][0]).pageNumber] = myCode.pagesList[i];
                            }
                            [myCode storeInDefaults];
                            [self.codeExecuteList addObject:myCode];
                        }
                        else
                        {
                            [self verifyPrioritiesFromCodes:myCode];
                            largeCode = YES;
                        }
                        myCode.willExecute = NO;
                    }
                }
            }
            if (self.codeExecuteList.count == 0)
                time++; 
        }
        while (self.codeExecuteList.count == 0 && !self.finished && !largeCode);
    }
    if (time > 0)
    self.totalTime = time;
}

#pragma mark - Process Priority Compare

- (BOOL)verifyPrioritiesFromCodes:(FHSOCode *)myCode
{
    FHSOCode *codeFromExecuteList;
    FHSOCode *codeWithLessPriority;
    BOOL changed = NO;
    for (int i = 0; i < self.codeExecuteList.count; i++)
    {
        codeFromExecuteList = self.codeExecuteList[i];
        
        if ((codeFromExecuteList.pagesList.count + self.memory.pagesFree) >= myCode.pagesList.count && codeFromExecuteList.priority < myCode.priority)
        {
            codeWithLessPriority = codeFromExecuteList;
        }
    }
    if (codeWithLessPriority != nil)
    {
        codeWithLessPriority.lastTime = self.totalTime;
        
        [codeWithLessPriority storeInDefaults];
        
        [self.memory deallocMemoryPositionFromArrayWithIndex:codeWithLessPriority.getOcuppiedIndexPages];
        
        [self.suspendedList addObject:codeWithLessPriority];
        
        [self.codeExecuteList removeObject:codeWithLessPriority];
        
        for (int i = 0; i < myCode.pagesList.count; i++)
        {
             ((FHSOIndexToMemory*) myCode.pagesList[i][0]).labelList = myCode.labelList;
            ((FHSOIndexToMemory*) myCode.pagesList[i][0]).pageNumber = [self.memory getIndexFromFreePage];
            self.memory.memory[((FHSOIndexToMemory*) myCode.pagesList[i][0]).pageNumber] = myCode.pagesList[i];
        }
        myCode.waitingTime += self.totalTime - myCode.lastTime;
        [self.codeExecuteList addObject:myCode];
        changed = YES;
    }
    else
    {
        [self.suspendedList addObject:myCode];
    }
    [self adjustExecuteList];
    [self adjustSuspendedList];
    return changed;
}

- (BOOL)verifyPrioritesFromExecuteListComparesWithSuspendedList
{
    FHSOCode *codeFromExecuteList;
    FHSOCode *codeFromSuspendedList;
    BOOL changed = NO;
    for (int i = 0; i < self.suspendedList.count; i++)
    {
        codeFromSuspendedList = self.suspendedList[i];
        if (codeFromSuspendedList.pagesList.count <= self.memory.pagesFree)
        {
            for (int i = 0; i < codeFromSuspendedList.pagesList.count; i++)
            {
                 ((FHSOIndexToMemory*) codeFromSuspendedList.pagesList[i][0]).labelList = codeFromSuspendedList.labelList;
                ((FHSOIndexToMemory*) codeFromSuspendedList.pagesList[i][0]).pageNumber = [self.memory getIndexFromFreePage];
                self.memory.memory[((FHSOIndexToMemory*) codeFromSuspendedList.pagesList[i][0]).pageNumber] =codeFromSuspendedList.pagesList[i];
            }
            codeFromSuspendedList.waitingTime += self.totalTime - codeFromSuspendedList.lastTime;
            [codeFromSuspendedList storeInDefaults];
            [self.codeExecuteList addObject:codeFromSuspendedList];
            [self.suspendedList removeObject:codeFromSuspendedList];
            [self adjustExecuteList];
            [self adjustSuspendedList];
        }
        else
        {
            for (int j = 0; j < self.codeExecuteList.count; j++)
            {
                codeFromExecuteList = self.codeExecuteList[j];
                
                if (codeFromSuspendedList.priority > codeFromExecuteList.priority && codeFromSuspendedList.pagesList.count <= (codeFromExecuteList.pagesList.count + self.memory.pagesFree))
                {
                    codeFromExecuteList.lastTime = self.totalTime;
                    [codeFromExecuteList storeInDefaults];
                    
                    [self.memory deallocMemoryPositionFromArrayWithIndex:codeFromExecuteList.getOcuppiedIndexPages];
                    
                    [self.suspendedList addObject:codeFromSuspendedList];
                    
                    [self.codeExecuteList removeObject:codeFromExecuteList];
                    
                    for (int i = 0; i < codeFromSuspendedList.pagesList.count; i++)
                    {
                         ((FHSOIndexToMemory*) codeFromSuspendedList.pagesList[i][0]).labelList = codeFromSuspendedList.labelList;
                        ((FHSOIndexToMemory*) codeFromSuspendedList.pagesList[i][0]).pageNumber = [self.memory getIndexFromFreePage];
                        self.memory.memory[((FHSOIndexToMemory*) codeFromSuspendedList.pagesList[i][0]).pageNumber] =codeFromSuspendedList.pagesList[i];
                    }
                    codeFromSuspendedList.waitingTime += self.totalTime - codeFromSuspendedList.lastTime;
                    [codeFromSuspendedList storeInDefaults];
                    [self.codeExecuteList addObject:codeFromSuspendedList];
                    [self.suspendedList removeObject:codeFromSuspendedList];
                    j = (int)self.codeExecuteList.count;
                    changed = YES;
                    [self adjustExecuteList];
                    [self adjustSuspendedList];
                }
            }
            
        }
    }
    return changed;
}

#pragma mark - Adjust Lists

- (void)adjustSuspendedList
{
    FHSOCode *firstCode;
    FHSOCode *secondCode;
    for (int i = 0; i < self.suspendedList.count; i++)
    {
        for (int j = 0; j < self.suspendedList.count-1; j++)
        {
            firstCode = self.suspendedList[j];
            secondCode = self.suspendedList[j+1];
            if (firstCode.priority < secondCode.priority)
            {
                FHSOCode *aux = firstCode;
                self.suspendedList[j] = secondCode;
                self.suspendedList[j+1] = aux;
            }
        }
    }
}

- (void)adjustExecuteList
{
    FHSOCode *myCode = [self.codeExecuteList firstObject];
    if (myCode != nil)
    {
        if (self.processExecutionTime == self.quantum)
        {
            myCode.isCurrent = NO;
            myCode.lastTime = self.totalTime;
            [myCode storeInDefaults];
            [self.codeExecuteList removeObject:myCode];
            [self.codeExecuteList addObject:myCode];
            FHSOCode *secondCode = (FHSOCode*)[self.codeExecuteList firstObject];
            secondCode.waitingTime += self.totalTime - secondCode.lastTime;
            [secondCode storeInDefaults];
            self.processExecutionTime = 0;
        }
        [self eliminateCodeFromExecuteList:myCode];
    }
}

- (void)eliminateCodeFromExecuteList:(FHSOCode*)myCode
{
    if (myCode != nil)
    {
        if (myCode.programCounter == myCode.instructionList.count || myCode.instructionList[myCode.programCounter] == [myCode.instructionList lastObject])
        {
            if (self.codeExecuteList.count > 1)
            {
                FHSOCode *secondCode = self.codeExecuteList[1];
                secondCode.waitingTime += self.totalTime - secondCode.lastTime;
                [secondCode storeInDefaults];
            }
            
            [self saveStatisctisFromCode:myCode];
            [myCode resetSettings];
            [self.memory deallocMemoryPositionFromArrayWithIndex: myCode.getOcuppiedIndexPages];
            [myCode eraseMemoryPosition];
            [myCode storeInDefaults];
            [self.codeExecuteList removeObject: myCode];
            self.processExecutionTime = 0;
        }
        [self managerExecuteList];
    }
}


#pragma mark - Get Code

- (FHSOCode*)getCodeToExecute
{
    if (self.codeExecuteList.count > 0)
    {
        FHSOCode *myCode = [self.codeExecuteList firstObject];
        [myCode setIsCurrent:YES];
        [myCode storeInDefaults];
        return myCode;
    }
    self.finished = YES;
    [self.memory removeFromDefaults];
    [self saveExecuteListInDefaults];
    return nil;
}

- (void)saveExecuteListInDefaults
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.codeExecuteList];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"executeList"];
    NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:self.suspendedList];
    [[NSUserDefaults standardUserDefaults] setObject:data2 forKey:@"suspendedList"];
    NSData *data3 = [NSKeyedArchiver archivedDataWithRootObject:self.codeStatisctsList];
    [[NSUserDefaults standardUserDefaults] setObject:data3 forKey:@"statisctsList"];

}

- (void)saveStatisctisFromCode:(FHSOCode *)myCode
{
    FHSOCodeStatistics *statiscts = [[FHSOCodeStatistics alloc] init];
    statiscts.name = myCode.name;
    statiscts.description = myCode.description;
    statiscts.waitingTime = myCode.waitingTime;
    statiscts.executeTime = myCode.executeTime;
    statiscts.processTime = myCode.processTime;
    statiscts.acumulator = myCode.acumulator;
    [self.codeStatisctsList addObject:statiscts];
}


@end
