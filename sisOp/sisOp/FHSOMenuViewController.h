//
//  FHSOMenuViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 3/25/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSOMenuViewController : UIViewController <UIScrollViewDelegate>

@end
