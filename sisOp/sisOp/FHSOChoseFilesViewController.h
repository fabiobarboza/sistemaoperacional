//
//  FHSOChoseFilesViewController.h
//  sisOp
//
//  Created by Fabio Barboza on 4/2/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHSOChoseFilesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
